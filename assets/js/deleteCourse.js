let courseId = params.get('courseId')



fetch(`http://localhost:3000/api/courses/${courseId}`)
.then(res => {
    return res.json()
})
.then(data => {
	

	document.querySelector("#deleteCourse").addEventListener("submit", (e) => {
		e.preventDefault()

		
		let token = localStorage.getItem('token')

		fetch('http://localhost:3000/api/courses', {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }, 
            body: JSON.stringify({
                isAdmin: false
            })
        })
        
	})
})
