let  profileContainer = document.querySelector("#profileContainer")

let token = localStorage.getItem('token');
let isAdmin = localStorage.getItem('isAdmin');
let emrollments = localStorage.getItem('enrollments');


if(isAdmin !== true){
	
	fetch('http://localhost:3000/api/users/details', {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		}
	})
	.then(res => res.json())
	.then(data => {
		profileContainer.innerHTML = `<h1>${data.firstName + " "+ data.lastName}</h1>
			<div class="col-md-12">
				<h3>${data.email}</h3>
				<h3>${data.mobileNo}</h3>
				</div>
			<div class="col-md-12">
				<h2>Enlisted Courses</h2>
				<h3>${data.enrollments}</h3>
			</div>`
	})

}else{
	fetch('http://localhost:3000/api/users/details', {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		}
	})
	.then(res => res.json())
	.then(data => {
		profileContainer = `<h1>${data.firstName + " "+ data.lastName}</h1>
			<div class="col-md-12">
				<h3>${data.email}</h3>
				<h3>${data.mobileNo}</h3>
				</div>`
	})
}